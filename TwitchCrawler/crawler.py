# -*- coding: utf-8 -*-
try:
    import requests
except ImportError:
    raise ImportError('É nescesário o módulo requests para a execução do TwitchCrawler')
import time

def getEndPoint(func):
	'''
		Retorna a API EndPoint de acordo com a função solicitada

		func -- A função que se deseja acessa na api
	'''
	if(func == 'streams'):
		return 'https://api.twitch.tv/helix/streams'
	if(func == 'game'):
		return 'https://api.twitch.tv/helix/games'
	if(func == 'user'):
		return 'https://api.twitch.tv/helix/users'
	if(func == 'follow'):
		return 'https://api.twitch.tv/helix/users/follows'
	if(func == 'VODs'):
		return 'https://api.twitch.tv/helix/videos'
	if(func == 'clips'):
		return 'https://api.twitch.tv/helix/clips'
	return None

def getStreams(minViews, clientID):
	'''
		Esta função retorna uma lista de Dicionários(Dict) contendo as Streams onlines no momento

		minViews -- O número mínimo de views que a stream deve ter para entrar na lista
		clientID -- O seu Client ID disponibilizado pela Twitch
	'''
	url = getEndPoint('streams')
	headers = {'Client-ID': clientID}
	parametros = {'first': 100}

	try:
		r = requests.get(url, headers=headers, params=parametros)
	except Exception as e:
		return None

	if(r.status_code != requests.codes.ok):
		return None

	final = r.json()['data']
	pagina = r.json()['pagination']['cursor']
	parametros = {'first': 100, 'after': pagina}

	while(final[-1]['viewer_count'] >= minViews):
		try:
			r = requests.get(url, headers=headers, params=parametros)
		except Exception as e:
			return None
		if(r.status_code != requests.codes.ok):
			break
		if(len(r.json()['data']) == 0):
			break
		final = final + r.json()['data']
		pagina = r.json()['pagination']['cursor']
		parametros = {'first': 100, 'after': pagina}

	for x in final:
		x['momento'] = int(time.time())

	return final

def getGameInfos(id, clientID):
	'''
		Esta função retorna uma descrição (dict) de um jogo cujo Id foi passado

		id -- O ID númerico do jogo que se deseja saber informações
		clientID -- O seu Client ID disponibilizado pela Twitch
	'''
	if(id == '0'):
		return {"id": "0", "name": "NÃO DEFINIDO", "box_art_url": ""}

	url = getEndPoint('game')
	headers = {'Client-ID': clientID}
	parametros = {'id': id}

	try:
		r = requests.get(url, headers=headers, params=parametros)
	except Exception as e:
		return None

	if(r.status_code != requests.codes.ok or len(r.json()['data']) == 0):
		return None

	infos = r.json()['data'][0]

	return infos

def getUserInfos(id, clientID):
	'''
		Esta função retorna uma descrição (dict) de um Streamer cujo Id foi passado

		id -- O ID númerico do streamer que se deseja saber informações
		clientID -- O seu Client ID disponibilizado pela Twitch
	'''
	url = getEndPoint('user')
	headers = {'Client-ID': clientID}
	parametros = {'id': id}

	try:
		r = requests.get(url, headers=headers, params=parametros)
	except Exception as e:
		return None

	if(r.status_code != requests.codes.ok or len(r.json()['data']) == 0):
		return None

	infos = r.json()['data'][0]

	return infos

def getUserInfosByName(login, clientID):
	'''
		Esta função retorna uma descrição (dict) de um Streamer cujo Nome de usuário foi passado

		login -- O Nome de usuário do streamer que se deseja saber informações
		clientID -- O seu Client ID disponibilizado pela Twitch
	'''
	url = getEndPoint('user')
	headers = {'Client-ID': clientID}
	parametros = {'login': login}

	try:
		r = requests.get(url, headers=headers, params=parametros)
	except Exception as e:
		return None

	if(r.status_code != requests.codes.ok or len(r.json()['data']) == 0):
		return None

	infos = r.json()['data'][0]

	return infos

def getNFollowers(id, clientID):
	'''
		Esta função retorna o número de seguidores de um streamer

		id -- O ID do streamer
		clientID -- O seu Client ID disponibilizado pela Twitch
	'''
	url = getEndPoint('follow')
	headers = {'Client-ID': clientID}
	parametros = {'id': id}

	try:
		r = requests.get(url, headers=headers, params=parametros)
	except Exception as e:
		return None

	if(r.status_code != requests.codes.ok):
		return None

	return r.json()['total']

def getVODs(id, n, clientID):
	'''
		Esta função retorna uma lista de Dicionários(Dict) contendo as Streams salvas de um streamer

		id -- O ID do Streamer
		n -- O número mínimo de vods para entrar na lista (Use 0 para retornar todos)
		clientID -- O seu Client ID disponibilizado pela Twitch
	'''
	url = getEndPoint('VODs')
	headers = {'Client-ID': clientID}
	parametros = {'user_id': id, 'first': 100}

	try:
		r = requests.get(url, headers=headers, params=parametros)
	except Exception as e:
		return None

	if(r.status_code != requests.codes.ok):
		return None

	final = r.json()['data']
	pagina = r.json()['pagination']['cursor']
	parametros = {'user_id': id, 'first': 100, 'after': pagina}

	while(len(final) <= n):
		try:
			r = requests.get(url, headers=headers, params=parametros)
		except Exception as e:
			return None

		if(r.status_code != requests.codes.ok):
			return None
		if(len(r.json()['data']) == 0):
			break
		final = final + r.json()['data']
		pagina = r.json()['pagination']['cursor']
		parametros = {'user_id': id, 'first': 100, 'after': pagina}

	if(len(final) > n):
		return final[0:n]

	return final

def getVODInfos(id, clientID):
	'''
		Esta função retorna uma descrição (dict) de um VOD cujo Id foi passado

		id -- O ID númerico do VOD que se deseja saber informações
		clientID -- O seu Client ID disponibilizado pela Twitch
	'''
	url = getEndPoint('VODs')
	headers = {'Client-ID': clientID}
	parametros = {'id': id}

	try:
		r = requests.get(url, headers=headers, params=parametros)
	except Exception as e:
		return None

	if(r.status_code != requests.codes.ok or len(r.json()['data']) == 0):
		return None

	infos = r.json()['data'][0]

	return infos

def getClips(id, n, clientID):
	'''
		Esta função retorna uma lista de Dicionários(Dict) contendo os Clips salvas de um streamer

		id -- O ID do Streamer
		n -- O número mínimo de vods para entrar na lista (Use 0 para retornar todos)
		clientID -- O seu Client ID disponibilizado pela Twitch
	'''
	url = getEndPoint('clips')
	headers = {'Client-ID': clientID}
	parametros = {'broadcaster_id': id, 'first': 100}

	try:
		r = requests.get(url, headers=headers, params=parametros)
	except Exception as e:
		return None

	if(r.status_code != requests.codes.ok):
		return None

	final = r.json()['data']
	pagina = r.json()['pagination']['cursor']
	parametros = {'broadcaster_id': id, 'first': 100, 'after': pagina}

	while(len(final) <= n):
		try:
			r = requests.get(url, headers=headers, params=parametros)
		except Exception as e:
			return None

		if(r.status_code != requests.codes.ok):
			return None
		if(len(r.json()['data']) == 0):
			break
		final = final + r.json()['data']
		pagina = r.json()['pagination']['cursor']
		parametros = {'broadcaster_id': id, 'first': 100, 'after': pagina}

	if(len(final) > n):
		return final[0:n]

	return final
