# -*- coding: utf-8 -*-
try:
	import pymongo
except ImportError:
	raise ImportError('É nescesário o módulo pymongo e pymongo[srv] para a execução do TwitchCrawler.persistenciaMongo')
import TwitchCrawler.tools as tool

def getConection(uri):
	'''
		Esta função retorna uma conexão com um banco mongoDB via mongodb+srv:

		uri -- a URL de conexão com o banco incluindo login e senha
	'''
	return pymongo.MongoClient(uri)

def inserirLive(live, con):
	'''
		Esta função insere uma de live(Dict) no banco de dados

		live -- A live retornada pela API da Twitch com o acrécimo da chave 'momento'
		con -- Uma conexão com o banco pré-estabelecida

		Esta função retorna True se a live não estava no banco anteriormente e False caso contrário
	'''
	db = con.TwitchUs
	col = db.logLives

	encontradas = col.find({"id": str(live["id"])})
	if(encontradas.count() == 0): # Este Id de Live não estava no banco
		col.insert_one(tool.compactarLive(live)) # Formatando e inserindo no banco
		return True
	else: # Esta live já está no banco
		linha = encontradas[0]["linhaDoTempo"] # Pegando a linha do tempo já existente
		linha.append(tool.getPontoNoTempo(live)) # Adicionando novo ponto na linha do tempo
		col.update_one({"id": live["id"]}, {"$set": {"linhaDoTempo": linha}}, upsert=False) # Atualizando no banco
		return False

def inserirJogo(jogo, con):
	'''
		Esta função insere um jogo(Dict) no banco

		jogo -- O jogo a ser inserido
		con -- Uma conexão com o banco pré-estabelecida

		Esta função retorna True se o jogo não estava no banco anteriormente e False caso contrário
	'''
	db = con.TwitchUs
	col = db.jogos

	encontrados = col.find({"id": str(jogo["id"])})
	if(encontrados.count() == 0): # Este Id de jogo não estava no banco
		col.insert_one(jogo)
		return True
	else: # Este jogo já está no banco
		col.update_one({"id": jogo["id"]}, {"$set": jogo}, upsert=False) # Atualizando no banco
		return False

def inserirStreamer(streamer, con):
	'''
		Esta função insere um streamer(Dict) no banco

		streamer -- O streamer a ser inserido
		con -- Uma conexão com o banco pré-estabelecida

		Esta função retorna True se o streamer não estava no banco anteriormente e False caso contrário
	'''
	db = con.TwitchUs
	col = db.users

	encontrados = col.find({"id": str(streamer["id"])})
	if(encontrados.count() == 0): # Este Id de streamer não estava no banco
		col.insert_one(streamer)
		return True
	else: # Este streamer já está no banco
		col.update_one({"id": streamer["id"]}, {"$set": streamer}, upsert=False) # Atualizando no banco
		return False

def inserirVOD(VOD, con):
	'''
		Esta função insere um VOD(Dict) no banco

		VOD -- As informações do Vídeo On Demand em questão
		con -- Uma conexão com o banco pré-estabelecida

		Esta função retorna True se o VOD não estava no banco anteriormente e False caso contrário
	'''
	db = con.TwitchUs
	col = db.VOD

	encontrados = col.find({"id": str(VOD["id"])})
	if(encontrados.count() == 0): # Este Id de VOD não estava no banco
		col.insert_one(VOD)
		return True
	else: # Este VOD já está no banco
		col.update_one({"id": VOD["id"]}, {"$set": VOD}, upsert=False) # Atualizando no banco
		return False

def getJogoById(id, con):
	'''
		Esta função retorna um jogo (Dict) de acordo com o id passado

		id -- O ID que se deseja buscar o jogo
		con -- Uma conexão com o banco pré-estabelecida
	'''
	db = con.TwitchUs
	col = db.jogos
	result = col.find_one({"id": str(id)})

	if(result != None):
		del(result['_id'])

	return result

def getUserById(id, con):
	'''
		Esta função retorna um streamer (Dict) de acordo com o id passado

		id -- O ID que se deseja buscar o streamer
		con -- Uma conexão com o banco pré-estabelecida
	'''
	db = con.TwitchUs
	col = db.users
	result = col.find_one({"id": str(id)})

	if(result != None):
		del(result['_id'])

	return result

def getVODById(id, con):
	'''
		Esta função retorna um VOD (Dict) de acordo com o id passado

		id -- O ID que se deseja buscar o VOD
		con -- Uma conexão com o banco pré-estabelecida
	'''
	db = con.TwitchUs
	col = db.VOD
	result = col.find_one({"id": str(id)})

	if(result != None):
		del(result['_id'])

	return result

def setVOD(idLive, idVOD, con):
	'''
	 Dado um id de uma live previamente inserida no banco esta função atualzia qual seu VOD

	 idLive -- O id da live desejada
	 con -- Uma conexão com o banco pré-estabelecida

	 Esta função retorna False se a live não esteja no banco True caso contrário
	'''
	db = con.TwitchUs
	col = db.logLives

	try:
		r = col.update_one({"id": idLive}, {"$set": {"VOD": idVOD}}, upsert=False) # Atualizando no banco
		return r.matched_count > 0
	except:
		return False

def deleteLive(idLive, con):
	'''
	 Dado um id de uma live previamente inserida no banco esta função a remove

	 idLive -- O id da live desejada
	'''
	db = con.TwitchUs
	col = db.logLives

	try:
		r = col.delete_one({"id": idLive})
		return r.deleted_count > 0
	except:
		return False

def getLiveById(id, con):
	'''
		Esta função retorna uma live (Dict) de acordo com o id passado

		id -- O ID que se deseja buscar a live
		con -- Uma conexão com o banco pré-estabelecida
	'''
	db = con.TwitchUs
	col = db.logLives
	result = col.find_one({"id": str(id)})

	return result

def getLiveByUserId(id, con):
	'''
		Esta função retorna uma live (Dict) de acordo com o id do streamer

		id -- O ID que do streamer se deseja buscar a live
		con -- Uma conexão com o banco pré-estabelecida
	'''
	db = con.TwitchUs
	col = db.logLives
	result = col.find({"user_id": str(id)})

	return list(result)

def getLivesIds(con):
	'''
		Esta função retorna todos os ids de lives salvos no banco em uma List

		con -- Uma conexão com o banco pré-estabelecida
	'''
	db = con.TwitchUs
	col = db.logLives
	lives = col.find({})

	ids = list()

	for live in lives:
		if(not (live["id"] in ids)):
			ids.append(live["id"])

	return ids
