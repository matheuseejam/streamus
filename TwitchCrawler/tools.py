# -*- coding: utf-8 -*-
try:
	import requests
except ImportError:
	raise ImportError('É nescesário o módulo requests para a execução do TwitchCrawler')
import datetime, calendar
import json

def getLimites(clientID):
	'''
		Esta função retorna uma lista de Dicionários(Dict) contendo:
		Limite: "A quantidade máxima de requests permitida para esse Client ID"
		Faltam: "Quantas você ainda pode fazer antes do Reset"
		Reset: "Unix Timestamp de quando vai resetar"
		Status: "O status atual das requests feitas"

		clientID -- O seu Client ID disponibilizado pela Twitch
	'''
	url = 'https://api.twitch.tv/helix/streams'
	headers = {'Client-ID': clientID}
	parametros = {'first': 1}

	try:
		r = requests.get(url, headers=headers, params=parametros)
	except Exception as e:
		return None

	data = {
		'Status': r.status_code,
		'Limite': int(r.headers['Ratelimit-Limit']),
		'Faltam': int(r.headers['Ratelimit-Remaining']),
		'Reset': int(r.headers['Ratelimit-Reset']),
	}

	return data

def vodDeletado(idVOD, clientID):
	'''
		Esta função retorna um se o vod passado ainda existe no sistema da Twitch ou foi deletado

		clientID -- O seu Client ID disponibilizado pela Twitch
		idVOD -- Id do vod a ser checado
	'''
	url = 'https://api.twitch.tv/helix/videos'
	headers = {'Client-ID': clientID}
	parametros = {'id': idVOD}

	try:
		r = requests.get(url, headers=headers, params=parametros)
	except Exception as e:
		return False

	try:
		if(r.json()['status'] == 404):
			return True
	except KeyError as e:
		return False

	return False

def getVODFromLive(live, clientID):
	'''
		Esta função retorna um Dict contendo o VOD que "Provavelmente" representa a live online no momento da chamada

		live -- As informações da live (Dict)
		clientID -- O seu Client ID disponibilizado pela Twitch
	'''
	url = 'https://api.twitch.tv/helix/videos'
	headers = {'Client-ID': clientID}
	parametros = {'user_id': live['user_id'], 'first': 100}

	try:
		r = requests.get(url, headers=headers, params=parametros)
	except Exception as e:
		return None

	if(r.status_code != requests.codes.ok):
		return None

	vods = r.json()['data']
	vodsFiltrados = [x for x in vods if x['thumbnail_url'] == ""] # Filtrando vods que não tem Thumbnail

	if(len(vodsFiltrados) == 0):
		return None

	return vodsFiltrados[0]

def compactarLives(lives):
	'''
		Esta função recebe uma lista de lives e compacta para eliminar repetição de dados

		lives -- a lista de lives orginais retornadas pela twitch
	'''
	compactadas = compactarLive(lives[0]) # Compactando a primeira

	for live in lives[1:]: # Ignorando a primeira que já está na lista
		compactadas["linhaDoTempo"].append(getPontoNoTempo(live))

	compactadas["linhaDoTempo"] = sorted(compactadas["linhaDoTempo"], key=lambda x: x["momento"])

	return compactadas

def serializarLives(lives):
	'''
		Esta função recebe uma lista de lives e retorna a lista formatada em Json

		lives -- a lista de lives orginais retornadas pela twitch
	'''
	final = list()
	for live in lives:
		try:
			del live['_id']
		except KeyError:
			pass
		final.append(live)

	return json.dumps(final)

def compactarLive(liveOriginal):
	'''
		Esta função recebe uma live(Dict) simples retornada pela api da Twitch e retorna compactada

		liveOriginal -- a live orginal retornada pela twitch
	'''
	VOD = None
	if("VOD" in liveOriginal): # Se o vod estiver presente
		VOD = liveOriginal["VOD"]

	if("linhaDoTempo" in liveOriginal): # Se já for uma live compactada
		return liveOriginal.copy() # Retorne a mesma sem modificações
	return {
		"id": liveOriginal["id"],
		"user_id": liveOriginal["user_id"],
		"VOD": VOD,
		"community_ids": liveOriginal["community_ids"],
		"type": liveOriginal["type"],
		"title": liveOriginal["title"],
		"started_at": twitchTimeToUnix(liveOriginal["started_at"]),
		"language": liveOriginal["language"],
		"thumbnail_url": liveOriginal["thumbnail_url"],
		"linhaDoTempo": [{"game_id": liveOriginal["game_id"], "momento": liveOriginal["momento"], "viewer_count": liveOriginal["viewer_count"]}]
	}

def getPontoNoTempo(liveOriginal):
	'''
		Esta função recebe uma live(Dict) simples retornada pela api da Twitch e retorna um dict contendo o momento, views e idJogo

		liveOriginal -- a live orginal retornada pela twitch
	'''
	return {"game_id": liveOriginal["game_id"], "momento": liveOriginal["momento"], "viewer_count": liveOriginal["viewer_count"]}

def twitchTimeToUnix(original):
	'''
		Esta função retorna um Unixtimestamp equivalente a data passada
		2018-09-10T19:29:40Z -> 1536618580

		original -- A data a ser convertida
	'''
	return int(calendar.timegm(datetime.datetime.strptime(original, "%Y-%m-%dT%H:%M:%SZ").timetuple()))

def aplicarFiltro(lives):
	'''
		Esta função retorna somente as lives em pt-br das lives passadas

		lives -- Lives a serem filtradas
	'''
	#			 Alanzoka      Gabi      BatataComPepino
	excecoes = ["47125717", "38244180",    "194389241"]
	# remova as lives que não são pt-br, ou não são do alanzoka(gambiarra)
	return [x for x in lives if x["language"] == "pt" or x["user_id"] in excecoes]

def jogosDaLive(live) :
	'''
		Esta função retorna a lista de jogos que foram jogados em uma live passada por parâmetro

		live --  A live a ser analizada
	'''
	linhaDoTempo = live["linhaDoTempo"]
	jogos = list()
	for momento in linhaDoTempo:
		if(not momento["game_id"] in jogos):
			jogos.append(momento["game_id"])

	return jogos

def jogosDasLives(lives):
	'''
		Esta função retorna a lista de jogos que foram jogados em um conjunto de lives passadas

		lives --  As lives a serem analizadas
	'''
	jogos = list()

	for live in lives:
		novos = jogosDaLive(live)
		for jogo in novos:
			if(not jogo in jogos):
				jogos.append(jogo)

	return jogos
