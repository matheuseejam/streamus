import TwitchCrawler.crawler as crl
import TwitchCrawler.tools as tool
import TwitchCrawler.persistenciaMongo as mongo
import time, json, os

twitchClienteID = 'xkiy482gril7xrtqjl779irqt05cfs'
con = mongo.getConection("mongodb+srv://matheus:hakunamatataeita@twitchus-6x2nm.mongodb.net/test?retryWrites=true")


'''
 Zona de funções
'''

def subirCrawlers():
	print("Iniciando crawler principal")
	os.system("nohup python3 crawler.py &> /dev/null &")
	print("Iniciando crawler auxiliar 1 (Jogos)")
	os.system("nohup python3 crawlerJogos.py &> /dev/null &")
	print("Iniciando crawler auxiliar 2 (Streamers)")
	os.system("nohup python3 crawlerUsers.py &> /dev/null &")
	print("Tudo feito !")

def downloadLives(nome):
	user = crl.getUserInfosByName(nome, twitchClienteID)

	lives = mongo.getLiveByUserId(user["id"], con)

	if(len(lives) == 0):
		print("Nenhuam live salva para o usuario "+nome)
		return False

	nomeArquivo = nome+"_lives.txt"
	arquivo = arquivo = open(nomeArquivo,"w+")
	arquivo.write(tool.serializarLives(lives))
	arquivo.close()

	print("Arquivo gerado: "+nomeArquivo+". com "+str(len(lives))+" lives")
	return True

def downloadInfoGame(id):
	jogo = mongo.getJogoById(id, con)

	if(jogo == None):
		print("Nenhum jogo encontrado no banco para este ID")
		return False

	nomeArquivo = str(id)+"_jogo.txt"
	arquivo = arquivo = open(nomeArquivo,"w+")
	arquivo.write(json.dumps(jogo))
	arquivo.close()

	print("Arquivo gerado: "+nomeArquivo+".")
	return True

def downloadbanco():
	listaChaves = mongo.getLivesIds(con)

	listaFinal = list()
	x = 0
	print("total: "+str(len(listaChaves)))
	for id in listaChaves:
		x = x +1
		print(x)
		listaFinal.append(mongo.getLiveById(id, con))

	arquivo = arquivo = open("banco.txt","w+")
	arquivo.write(tool.serializarLives(listaFinal))
	arquivo.close()
	print("Operação concluida !")

def downloadGames():
	listaChaves = mongo.getLivesIds(con)

	listaFinal = list()
	x = 0
	print("total: "+str(len(listaChaves)))
	for id in listaChaves:
		x = x +1
		print(x)
		listaFinal.append(mongo.getLiveById(id, con))

	jogos = tool.jogosDasLives(listaFinal)

	arquivo = arquivo = open("jogos.txt","w+")
	arquivo.write(json.dumps(jogos))
	arquivo.close()
	print("Operação concluida !")

def limparBanco():
	db = con.TwitchUs
	col = db.logLives

	col.drop()

	print("Operação concluida !")

'''
####### ZONA DE ITERAÇÃO COM O USUARIO #######
'''

print("\n> Bem Vindo a interface de controle <")

continuar = True
while (continuar):
	op = int(input('''
		Escolha uma opção:
		1 - Baixar Streams salvas de um Streamer
		2 - Baixar todo o banco de dados (Operação pesada)
		3 - Baixar todos os jogos das lives até agora (Operação pesada)
		4 - Deletar todas os logs de lives salvas (Cuidado!)
		5 - Baixar dados de um jogo
		0 - Sair

		> '''))

	if(op == 0):
		continuar = False
	elif(op == 1):
		downloadLives(input("Insira o login do Streamer: "))
	elif(op == 2):
		downloadbanco()
	elif(op == 3):
		downloadGames()
	elif(op == 4):
		limparBanco()
	elif(op == 5):
		downloadInfoGame(input("Insira o id do jogo: "))
	else:
		print("Opção inválida\n")
