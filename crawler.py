import TwitchCrawler.crawler as crl
import TwitchCrawler.tools as tool
import TwitchCrawler.persistenciaMongo as mongo
import time


twitchClienteID = 'xkiy482gril7xrtqjl779irqt05cfs'
con = mongo.getConection("mongodb+srv://matheus:hakunamatataeita@twitchus-6x2nm.mongodb.net/test?retryWrites=true")

def vodExistente(idVOD):
	db = con.TwitchUs
	col = db.logLives
	result = col.find_one({"VOD": str(idVOD)})

	if(result != None):
		True
	return False

print("> Iniciando logger")

while(True):
	popularStreams = crl.getStreams(200, twitchClienteID) # Pegando todas as Streams com mais de 200 pessoas assistindo

	if(popularStreams == None): # Se não foi possível baixar as lives
		print("Atenção! Não foi possível obter a lista de lives.")
		popularStreams = list() # Setando como uma lista de lives vazia

	livesFiltradas = tool.aplicarFiltro(popularStreams) #Filtrando as lives

	for live in livesFiltradas: # Percorrendo a lista de lives já filtrada
		liveNova = mongo.inserirLive(live, con) # Inserindo no banco

		if(liveNova): # Esta é uma live nova, não estava no banco antes
			VOD = tool.getVODFromLive(live, twitchClienteID) # Descobrindo qual o VOD da nova live

			if(VOD == None): # Falha ao obter o VOD, talvez o limite de requests a API tenha excedido
				print("Falha ao obter o VOD da nova live")
				limites = tool.getLimites(twitchClienteID) # Obtendo as informações de limites de consulta a API
				if(limites == None):
					print("Falha na conexão com a API")
				elif(limites["Faltam"] == 0):
					print("O limite de consulta a API foi Excedido. Aguardando um minuto...")
					time.sleep(60)
					VOD = tool.getVODFromLive(live, twitchClienteID) # Tentando de novo
				else:
					print("Erro desconhecido ao obter o VOD")

			if(VOD != None and not vodExistente(VOD["id"])): # Conseguiu o vod
				if(not mongo.setVOD(live["id"], VOD["id"], con)): # Gravando a informação de qual o VOD da nova live
					print("Não foi possível setar o VOD("+str(VOD["id"])+") da live("+str(live["id"])+")\n")
			else: # Não conseguiu o VOD
				print("Não foi possível obter o VOD da live("+str(live["id"])+")\n")
				mongo.deleteLive(live["id"], con) # Se o VOD as informações da live são inuteis

	print("\n"+str(len(livesFiltradas))+" logs inseridos no banco\n") # Printando só pra avisar no terminal

	time.sleep(60) # Execute essa operação a cada um minuto
