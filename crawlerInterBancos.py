import pymysql
import TwitchCrawler.persistenciaMongo as mongo

'''
	INICIO DA ZONA DE FUNÇÕES
'''
def inserirJogo(cursor, jogo):
	try:
		cursor.execute("INSERT INTO Jogo VALUES (%s,%s,%s);", (jogo["id"], jogo["name"], jogo["box_art_url"]))
	except pymysql.err.IntegrityError as e:
		return False
	return True

def inserirJogos(cursorSQL, conMongo):
	cursorMongo = conMongo.TwitchUs.jogos.find({}) # Obterndo cursor para todos os jogos

	nJogos = conMongo.TwitchUs.jogos.count_documents({})
	print("\n###Iniciando transferencia dos jogos###\n")
	print(">> Inserindo {} jogos".format(nJogos))

	nSucessos = 0
	nErros = 0
	for loop in range(1, nJogos+1):
		i = nJogos-loop
		atual = cursorMongo[i]
		if(inserirJogo(cursorSQL, atual)):
			nSucessos += 1
		else:
			nErros += 1
		print("({}/{}) | Sucessos: {} | Falhas: {}".format(loop, nJogos, nSucessos, nErros), end='\r')
	print("\n")


def inserirUser(cursor, user):
	try:
		cursor.execute("INSERT INTO Usuario VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s);", (user["id"], user["login"], user["display_name"], user["type"], user["broadcaster_type"], user["description"], user["profile_image_url"], user["offline_image_url"], user["view_count"]))
	except pymysql.err.IntegrityError as e:
		return False
	return True

def inserirUsers(cursorSQL, conMongo):
	cursorMongo = conMongo.TwitchUs.users.find({}) # Obterndo cursor para todos os jogos

	nUsers = conMongo.TwitchUs.users.count_documents({})
	print("\n###Iniciando transferencia dos Streamers###\n")
	print(">> Inserindo {} Streamers".format(nUsers))

	nSucessos = 0
	nErros = 0
	for loop in range(1, nUsers+1):
		i = nUsers-loop
		atual = cursorMongo[i]
		if(inserirUser(cursorSQL, atual)):
			nSucessos += 1
		else:
			nErros += 1
		print("({}/{}) | Sucessos: {} | Falhas: {}".format(loop, nUsers, nSucessos, nErros), end='\r')
	print("\n")

def inserirMomento(cursorSQL, momento, id_stream):
	try:
		cursorSQL.execute("INSERT INTO Momento VALUES (%s,%s,%s,%s);", (id_stream, momento["game_id"], momento["momento"], momento["viewer_count"]))
	except pymysql.err.IntegrityError as e:
		return False
	return True

def inserirMomentos(cursorSQL, momentos, id_stream):
	for momento in momentos:
		inserirMomento(cursorSQL, momento, id_stream)

def inserirStream(cursor, stream):
	try:
		cursor.execute("INSERT INTO Stream VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);", (stream["id"], stream["user_id"], stream["user_name"], stream["type"], stream["title"], stream["description"], stream["created_at"], stream["published_at"], stream["started_at"], stream["url"], stream["thumbnail_url"], stream["viewable"], stream["view_count"], stream["language"], stream["duration"]))
		inserirMomentos(cursor, stream["linhaDoTempo"], stream["id"])
	except pymysql.err.IntegrityError as e:
		return False
	return True

def inserirStreams(cursorSQL, conMongo):
	cursorMongo = conMongo.TwitchUs.VOD.find({}) # Obterndo cursor para todos os jogos

	nLives = conMongo.TwitchUs.VOD.count_documents({})
	print("\n###Iniciando transferencia das Lives###\n")
	print(">> Inserindo {} lives".format(nLives))

	nSucessos = 0
	nErros = 0
	for loop in range(1, nLives+1):
		i = nLives-loop
		atual = cursorMongo[i]
		if(inserirStream(cursorSQL, atual)):
			nSucessos += 1
		else:
			nErros += 1
		print("({}/{}) | Sucessos: {} | Falhas: {}".format(loop, nLives, nSucessos, nErros), end='\r')

'''
	FIM DA ZONA DE FUNÇÕES
'''

print("> Iniciando logger\n")
#COMEÇANDO

con = mongo.getConection("mongodb+srv://matheus:hakunamatataeita@twitchus-6x2nm.mongodb.net/test?retryWrites=true")
try:
	db = pymysql.connect("localhost", "root", "", "StreamUS")
except pymysql.err.OperationalError as e:
	print("Falha ao conectar com o Banco MySQL")
	exit()
cursorSQL = db.cursor()

inserirJogos(cursorSQL,con)
inserirUsers(cursorSQL,con)
inserirStreams(cursorSQL, con)

# FINALIZANDO
db.commit()
db.close()
print("\n\n> Fim da execução")
