import TwitchCrawler.crawler as crl
import TwitchCrawler.tools as tool
import TwitchCrawler.persistenciaMongo as mongo
import time


twitchClienteID = 'xkiy482gril7xrtqjl779irqt05cfs'
con = mongo.getConection("mongodb+srv://matheus:hakunamatataeita@twitchus-6x2nm.mongodb.net/test?retryWrites=true")


print("> Iniciando crawler de jogos")

arquivo = None
try:
	arquivo = open("ultimaLive_CrawlerJogos.txt", "r")
except FileNotFoundError as e:
	open("ultimaLive_CrawlerJogos.txt", "w+").close()
	arquivo = open("ultimaLive_CrawlerJogos.txt", "r")

bruto = arquivo.read()
arquivo.close()

ultima = None
if(bruto != ''):
	ultima = bruto

while(True):
	cursor = con.TwitchUs.VOD.find({}) 					# Obterndo cursor para todas os VODs
	tamanho = con.TwitchUs.VOD.count_documents({}) 		# Pegando a quantidade de lives
	if(tamanho == 0):
		break

	print("Iniciando novo percurso em "+str(tamanho)+" VODs")
	for loop in range(tamanho-1): 			# Percorrendo todas as lives
		indexInverso = (tamanho-1) - loop	# Percorrendo a lista de trás pra frente
		live = cursor[indexInverso]			# Pegando uma live da lista
		if(live["id"] == ultima):			# Se já chegamos a ultima live verificada
			break							# Pare de percorrer a lista

		listaJogosIDS = tool.jogosDaLive(live)				# Listando os ids de jogos jogados nessa live
		for idJogo in listaJogosIDS:						# Percorrendo a lista de ids
			if(mongo.getJogoById(idJogo, con) != None):		# Se o game já está jo banco
				continue									# Ignore e vá para o próximo
			jogo = crl.getGameInfos(idJogo, twitchClienteID)# Pegando informações sobre o id atual
			if(jogo != None):								# Se foi possível obter informações sobre este game
				print("Inserindo jogo "+jogo["name"])
				mongo.inserirJogo(jogo, con)				# Inserindo informações no banco
			else:											# Se não foi possível obter as informações
				print("Não foi possível obter informações obre o jogo "+idJogo+", esperando um minuto...")
				time.sleep(60)

	ultima = cursor[tamanho-1]["id"]								# Guarde qual foi a última deste percurso
	arq = open("ultimaLive_CrawlerJogos.txt", "w+")
	arq.write(ultima)
	arq.close()
	print("Dormindo 5min até a próxima iteração ZZzzZzZZzz")
	try:
		time.sleep(60*5)											# Dormindo 5min para dar tempo de acumular mais lives
	except KeyboardInterrupt:
		print("\n\nTchau Tchau")
		exit(0)
