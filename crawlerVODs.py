import TwitchCrawler.crawler as crl
import TwitchCrawler.tools as tool
import TwitchCrawler.persistenciaMongo as mongo
import time

twitchClienteID = 'xkiy482gril7xrtqjl779irqt05cfs'
con = mongo.getConection("mongodb+srv://matheus:hakunamatataeita@twitchus-6x2nm.mongodb.net/test?retryWrites=true")

def adicionarDados(live, VOD):
	return {
		'id': VOD["id"],
		'user_id': VOD["user_id"],
		'user_name': VOD["user_name"],
		'title': VOD["title"],
		'description': VOD["description"],
		'created_at': tool.twitchTimeToUnix(VOD["created_at"]),
		'published_at': tool.twitchTimeToUnix(VOD["published_at"]),
		'started_at': live["started_at"],
		'url': VOD["url"],
		'thumbnail_url': VOD["thumbnail_url"],
		'viewable': VOD["viewable"],
		'view_count': VOD["view_count"],
		'language': VOD["language"],
		'type': VOD["type"],
		'duration': VOD["duration"],
		'linhaDoTempo': live["linhaDoTempo"]
	}

def printarStatus(n, tamanho, nSucessos, nErros, nDuplicados):
	print("\r", end='')
	print("({}/{}) | Sucessos: {} | Falhas: {} | Duplicados: {}".format(n, tamanho, nSucessos, nErros, nDuplicados), flush=True, end='')

def gravarErros(erros):
	arq = open("VODsNotFound.txt", "w+")
	arq.write(erros)
	arq.close()

print("> Iniciando crawler de VODs")

while(True):
	cursor = con.TwitchUs.logLives.find({}) # Obterndo cursor para todas as lives
	tamanho = con.TwitchUs.logLives.count_documents({}) 		# Pegando a quantidade de lives
	if(tamanho == 0):
		break

	idsVODNotFound = ""

	n = 0
	nErros = 0
	nSucessos = 0
	nDuplicados = 0

	print("\nIniciando novo percurso em "+str(tamanho)+" lives")
	try:
		for live in cursor:											# Percorrendo todas as lives
			n += 1
			idVOD = live['VOD']										# Pegando o id do VOD desta live
			if(mongo.getVODById(idVOD, con) != None):				# Se o VOD já está jo banco
				nDuplicados += 1
				printarStatus(n, tamanho, nSucessos, nErros, nDuplicados)
				mongo.deleteLive(live["id"], con)					# Não faz sentido manter esta live se o VOD já pertence a outra
				continue											# Ignore e vá para o próximo

			VOD = crl.getVODInfos(idVOD, twitchClienteID)			# Pegando informações sobre o id atual
			if(VOD != None and VOD["thumbnail_url"] != ""):			# Se foi possível obter informações sobre este VOD e ele é válido
				VODIncrementado = adicionarDados(live, VOD)
				mongo.inserirVOD(VODIncrementado, con)				# Inserindo informações no banco
				mongo.deleteLive(live["id"], con)					# Deletando live
				nSucessos += 1
				printarStatus(n, tamanho, nSucessos, nErros, nDuplicados)
			else:													# Se não foi possível obter as informações
				limites = tool.getLimites(twitchClienteID) 			# Obtendo as informações de limites de consulta a API
				if(limites == None or limites["Faltam"] == 0):
					nErros += 1
					printarStatus(n, tamanho, nSucessos, nErros, nDuplicados)
					time.sleep(60)
				else:												# Não fui um problema na conexão com a api da twitch
					nErros += 1
					printarStatus(n, tamanho, nSucessos, nErros, nDuplicados)
					idsVODNotFound += "\n"+str(idVOD)
					if(tool.vodDeletado(idVOD, twitchClienteID)):	# Se foi por causa do VOD ter sido deletado
						mongo.deleteLive(live["id"], con)			# Delete essa live
					elif(( int(time.time()) - live["started_at"] ) > (60*60*24*7)): # Se esta live está a muito tempo no banco
						mongo.deleteLive(live["id"], con)			# Delete essa live
					gravarErros(idsVODNotFound)
	except:
		cursor = con.TwitchUs.logLives.find({})						# Refazendo o cursor perdido
		continue


	print("\nDormindo 5min até a próxima iteração ZZzzZzZZzz\n")
	try:
		time.sleep(60*5)											# Dormindo 5min para dar tempo de acumular mais lives
	except KeyboardInterrupt:
		print("\n\nTchau Tchau")
		exit(0)
